<?php
    require_once('Animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    $sheep = new Animal("shaun");
    echo "Nama Hewan: $sheep->name <br>";
    echo "Jumlah kaki: $sheep->legs <br>";
    echo "Cold Blooded: $sheep->cold_blooded <br><br>";


    $sungokong = new Ape("kera sakti");
    echo "Nama Hewan: $sungokong->name <br>";
    echo "Jumlah kaki: $sungokong->legs <br>";
    echo "Cold Blooded: $sungokong->cold_blooded <br>";
    echo $sungokong->yell(). "<br><br>";


    $kodok = new Frog("buduk");
    echo "Nama Hewan: $kodok->name <br>";
    echo "Jumlah kaki: $kodok->legs <br>";
    echo "Cold Blooded: $kodok->cold_blooded <br>";
    echo $kodok->jump();





?>